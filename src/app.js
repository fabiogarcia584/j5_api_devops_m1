const express = require('express');
const app = express();

const cities = ["Bordeaux", "Paris", "Lyon", "Strasbourg", "Marseille", "Toulouse"];

//TDV
app.get('/status', (req, res) => {
    res.status(200).send();
    res.end();
});

// CITY
app.get('/city', (req, res) => {
    res.status(200).json(cities);
    res.end();
});


//Capture All 404 errors
app.use(function (req, res, next) {
    res.status(404).send('Unable to find the requested resource!');
    res.end();
 });
 

app.listen(process.env.PORT || 3000, () => {
    console.log(`App started on port ${process.env.PORT || 3000}`);
});
